<?php

return [
    'providers' => [
        'url' => [
            '/login-status'               => [
                'controller' => Weblab\Auth\Controller\Auth::class,
                'view'       => 'loginStatus',
                'name'       => 'loginStatus',
            ],
            '/login'                      => [
                'controller' => Weblab\Auth\Controller\Auth::class,
                'view'       => 'login',
                'name'       => 'login',
            ],
            '/login/facebook'             => [
                'controller' => Weblab\Auth\Controller\Facebook::class,
                'view'       => 'login',
                'name'       => 'login_facebook',
            ],
            '/takelogin/facebook'         => [
                'controller' => Weblab\Auth\Controller\Facebook::class,
                'view'       => 'takelogin',
                'name'       => 'takelogin_facebook',
            ],
            '/logout'                     => [
                'controller' => Weblab\Auth\Controller\Auth::class,
                'view'       => 'logout',
            ],
            '/register'                   => [
                'controller' => Weblab\Auth\Controller\Auth::class,
                'view'       => 'register',
            ],
            '/activate-user/[activation]' => [
                'controller' => Weblab\Auth\Controller\Auth::class,
                'view'       => 'activate',
            ],
            '/forgot-password'            => [
                'controller' => Weblab\Auth\Controller\Auth::class,
                'view'       => 'forgotPassword',
            ],
            '/forgot-password/success'    => [
                'controller' => Weblab\Auth\Controller\Auth::class,
                'view'       => 'forgotPasswordSuccesful',
            ],
            '/forgot-password/error'      => [
                'controller' => Weblab\Auth\Controller\Auth::class,
                'view'       => 'forgotPasswordError',
            ],
        ],
    ],
];