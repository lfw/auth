<?php

namespace Weblab\Auth\Service\Provider;

use Pckg\Framework\Response;
use Pckg\Framework\Router;
use Weblab\Auth\Entity\Users;
use Weblab\Auth\Service\Auth;
use Weblab\Auth\Service\ProviderInterface;

class Database implements ProviderInterface
{

    protected $users;

    public function __construct(Users $users, Response $response, Router $router, Auth $auth)
    {
        $this->users = $users;
        $this->response = $response;
        $this->router = $router;
        $this->auth = $auth;
    }

    public function getUser()
    {

    }

    public function redirectToLogin()
    {
        $this->response->redirect($this->router->make('login'));
    }

    public function logout()
    {

    }


}