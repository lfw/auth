<?php

namespace Weblab\Auth\Provider;

/*
Registers commands, and middlewared on initialization
*/

use Pckg\Framework\Provider;
use Pckg\Framework\Request\Session\SessionUser;
use Weblab\Auth\Command;
use Weblab\Auth\Command\LoginUser;
use Weblab\Auth\Command\LogoutUser;
use Weblab\Auth\Command\RegisterUser;
use Weblab\Auth\Command\SendNewPassword;
use Weblab\Auth\Controller\Auth;
use Weblab\Auth\Controller\Facebook;
use Weblab\Auth\Event;
use Weblab\Auth\Event\UserLoggedIn;
use Weblab\Auth\Event\UserRegistered;
use Weblab\Auth\Middleware;
use Weblab\Auth\Middleware\HandleLoginRequest;
use Weblab\Auth\Middleware\HandleLogoutRequest;
use Weblab\Auth\Middleware\LoginWithCookie;
use Weblab\Auth\Middleware\RestrictAccess;

class Config extends Provider
{

    public function commands()
    {
        return [
            'user.loginUser'       => LoginUser::class,
            'user.logoutUser'      => LogoutUser::class,
            'user.registerUser'    => RegisterUser::class,
            'user.sendNewPassword' => SendNewPassword::class,
        ];
    }

    public function middlewares()
    {
        return [
            'auth.loginWithCookie'     => LoginWithCookie::class,
            'auth.restrictAccess'      => RestrictAccess::class,
            'auth.handleLogoutRequest' => HandleLogoutRequest::class,
            'auth.handleLoginRequest'  => HandleLoginRequest::class,
        ];
    }

    public function event()
    {
        return [
            'user.loggedIn'   => UserLoggedIn::class,
            'user.registered' => UserRegistered::class,
        ];
    }

    public function path()
    {
        return [
            'view' => realpath(__DIR__ . '/../View'),
        ];
    }

    public function view()
    {
        return [
            'object' => [
                '_user' => SessionUser::class,
            ],
        ];
    }

    public function routes()
    {
        return [
            'url' => $this->baseRoutes() + $this->facebookRoutes(),
        ];
    }

    protected function baseRoutes()
    {
        return array_merge_array([
            'controller' => Auth::class,
        ], [
            '/login-status'               => [
                'view' => 'loginStatus',
                'name' => 'loginStatus',
            ],
            '/login'                      => [
                'view' => 'login',
                'name' => 'login',
            ],
            '/logout'                     => [
                'view' => 'logout',
            ],
            '/register'                   => [
                'view' => 'register',
            ],
            '/activate-user/[activation]' => [
                'view' => 'activate',
            ],
            '/forgot-password'            => [
                'view' => 'forgotPassword',
            ],
            '/forgot-password/success'    => [
                'view' => 'forgotPasswordSuccessful',
            ],
            '/forgot-password/error'      => [
                'view' => 'forgotPasswordError',
            ],
        ]);
    }

    protected function facebookRoutes()
    {
        return array_merge_array([
            'controller' => Facebook::class,
        ], [
            '/login/facebook'     => [
                'view' => 'login',
                'name' => 'login_facebook',
            ],
            '/takelogin/facebook' => [
                'view' => 'takelogin',
                'name' => 'takelogin_facebook',
            ],
        ]);
    }

}