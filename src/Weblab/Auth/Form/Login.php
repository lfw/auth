<?php

namespace Weblab\Auth\Form;

use Pckg\Htmlbuilder\Element\Form\Bootstrap;
use Weblab\Auth\Form\Validator\UserEmail;

/**
 * Class Login
 * @package Weblab\Auth\Form
 */
class Login extends Bootstrap
{

    /**
     * @return $this
     */
    public function initFields()
    {
        $fieldset = $this->addFieldset();

        $fieldset->addEmail('email')
            ->setLabel('Email:')
            ->addValidator(new UserEmail())
            ->required();

        $fieldset->addPassword('password')
            ->setLabel('Password:')
            ->required();

        $this->addSubmit();

        return $this;
    }
}