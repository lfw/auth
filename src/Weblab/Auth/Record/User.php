<?php

namespace Weblab\Auth\Record;

use Pckg\Database\Record;
use Weblab\Auth\Entity\Users;
use Weblab\Auth\Service\Auth;

/**
 * Class User
 * @package Weblab\Auth\Record
 */
class User extends Record
{

    protected $entity = Users::class;

}