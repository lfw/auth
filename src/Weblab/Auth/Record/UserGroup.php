<?php

namespace Weblab\Auth\Record;

use Pckg\Database\Record;
use Weblab\Auth\Entity\UserGroups;

class UserGroup extends Record {

    protected $entity = UserGroups::class;

}